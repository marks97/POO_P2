/*
 * Authors: Rupesh Pons u137564 i Marc Amorós u138095
 *
 * Aquesta és la classe Position. És un objecte contenidor de dues variables,
 * x i y, les quals ens indiquen les coordenades de la posició d'un objecte en
 * un pla.
 *
*/

public class Position {

    private int x,y;

    public Position(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int X() {
        return x;
    }

    public int Y() {
        return y;
    }

}
