/*
 * Authors: Rupesh Pons u137564 i Marc Amorós u138095
 *
 * Aquesta és la classe Turtle. Com a paràmetre del constructor rebem  els paràmetres
 * inicials de la tortuga, que seran desats en un objecte Position, i l'angle inicial.
 *
 * Com a variables tenim una constant, una posició, on en tot moment podrem saber on es
 * troba la tortuga, l'angle cap a on es dirigirà aquesta, i un boolean que ens servirà
 * per saber si la tortuga ha de dibuixa al seu pas, o no.
 *
*/

import java.awt.Graphics ;

public class Logo {

    private final boolean turteView = false; //We set this variable only for debugger use

    //Attributes
    private final int width;
    private final int height;
    private Turtle t;

    //Methods
    public Logo (int w, int h){
        this.width = w;
        this.height = h;
    }

    //Getters and setters
    public int getWidth(){
        return width;
    }

    public int getHeight(){
        return height;
    }

    /*
     * This function set the Turtle to his original position and his original variables
     */

    public void resetTurtle(){

        int x = width/2;
        int y = height/2;

        t.setPosition(x , y);
        t.setAngle(0);
    }

    /*
     * This function executes the whole program. Creates an instance of the object
     * Turtle, and sends to it all the instructions of the Program. Also draw the turtle
     * on every move that it makes.
     */

    public void execute(Program p, Graphics g) {

        int x = width/2;
        int y = height/2;

        t = new Turtle(x , y, 0);
        t.turn(90);

        //We first check if the program is right
        if(p.isCorrect()) {
            p.restart();
            while(!p.hasFinished()) {
                //We loop this code until de program has finished
                Instruction inst = p.getNextInstruction();
                if(inst != null) {
                    if(inst.getCode().equals("FWD")) {
                        t.forward(inst.getParam(), g);
                    } else if (inst.getCode().equals("ROT")) {
                        t.turn(inst.getParam());
                    } else if (inst.getCode().equals("PEN")) {
                        if (inst.getParam() == 1) {
                            t.setPen(true);
                        } else {
                            t.setPen(false);
                        }
                    }
                }
                if(turteView)
                    t.draw(g); //Here we call a method to draw the turtle
            }
        } else
            p.printErrors();
    }
}