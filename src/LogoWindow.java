/*
 * Authors: Rupesh Pons u137564 i Marc Amorós u138095
 *
 * Aquesta és la classe LogoWindow. És la classe principal del programa, la qual
 * conté el JPanel on dibuixarem el logo.
 *
 * Aquí creem un logo (amb un conjunt de instruccions) i cridem a la funció execute,
 * passant-li com a paràmetre el programa que conté les instruccions per crear el logo.
 *
*/

import java.awt.Graphics;

public class LogoWindow extends javax.swing.JFrame {

    private Logo logo ;
    private Program p ;

    public LogoWindow() {

        initComponents(); //Here we call the auto-generated methods
        setSize (800, 600);

        logo = new Logo(800,600);

        p = new Program ("THREE_SQUARES");

        p.addInstruction("REP" , 3);
        p.addInstruction("PEN" , 1);
        p.addInstruction("REP" , 4);
        p.addInstruction("FWD" , 100);
        p.addInstruction("ROT" , 90);
        p.addInstruction("END" , 1);
        p.addInstruction("PEN", 0);
        p.addInstruction("FWD" , 120);
        p.addInstruction("END" , 1);


    }

    // Paint method for Graphics
    public void paint(Graphics g){
        super.paint(g);
        logo.execute(p, g);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */

    @SuppressWarnings("unchecked")
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
                jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 400, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
                jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 300, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }


    public static void main(String args[]) {

        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(LogoWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(LogoWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(LogoWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(LogoWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new LogoWindow().setVisible(true);
            }
        });
    }

    private javax.swing.JPanel jPanel1;

}