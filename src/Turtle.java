/*
 * Authors: Rupesh Pons u137564 i Marc Amorós u138095
 *
 * Aquesta és la classe Turtle. Com a paràmetre del constructor rebem  els paràmetres
 * inicials de la tortuga, que seran desats en un objecte Position, i l'angle inicial.
 *
 * Com a variables tenim una constant, una posició, on en tot moment podrem saber on es
 * troba la tortuga, l'angle cap a on es dirigirà aquesta, i un boolean que ens servirà
 * per saber si la tortuga ha de dibuixa al seu pas, o no.
 *
*/

import java.awt.Graphics;

public class Turtle {

    private static final int POLYGON_VERTICES = 3;

    private Position position;
    private double angle;
    private boolean pen = true;

    public Turtle(int x, int y, double angle) {
        position = new Position(x,y);
        this.angle = angle;
    }
    public Position getPosition(){
        return position;
    }

    public double getAngle(){
        return angle;
    }

    public void setAngle(double angle){
        this.angle = angle;
    }

    public void setPosition(int x, int y){
        this.position = new Position(x,y);
    }

    //Mètode perque la tortuga es mogui cap endavant

    public void forward (double distance, Graphics g){

        int x1 = getPosition().X(); //Obtenem la posició actual de la tortuga
        int y1 = getPosition().Y();
        int x2 = (int) (x1 + distance * Math.sin(getAngle())); //Obtenim la posició final de la tortuga
        int y2 = (int) (y1 + distance * Math.cos(getAngle())); //Usem trigonometria bàscia per calcular els punts finals

        if(isPenOn()) {
            g.drawLine(x1, y1, x2, y2); //Dibuixem la línia
        }

        setPosition(x2 , y2);
    }

    //Mètode per canviar la direcció de la tortuga (utilitzant graus)

    public void turn (double a){
        a = a * (Math.PI / 180);
        setAngle(a + getAngle());
    }

    //Posa el pen on "on" o "off" depentent del que se li passi per parametre

    public void setPen (boolean penState){
        this.pen = penState;
    }

    //Comprova si pen esta en "on" o en "off"

    public boolean isPenOn (){
        return pen;
    }

    //Aquest mètode serveix per imprimir per pantalla un triangle, que simbolitza la tortuga

    public void draw (Graphics g ){

        int []xc = new int[POLYGON_VERTICES];
        int []yc = new int [POLYGON_VERTICES];

        int x = position.X();
        int y = position.Y();

        double dirX = Math.sin(getAngle());
        double dirY = Math.cos(getAngle());

        xc[0] = (int)(x + 8 * dirY); yc[0]  = (int)(y - 8  * dirX);
        xc[1] = (int)(x - 8 * dirY); yc [1] = (int)(y + 8  * dirX);
        xc[2] = (int)(x + 16* dirX); yc [2] = (int)(y + 16 * dirY);

        g.drawPolygon(xc, yc, POLYGON_VERTICES);

    }

}
