/*
 * Authors: Rupesh Pons u137564 i Marc Amorós u138095
 *
 * Aquesta és la classe Instruction. Com a variables al constructor rebem un String,
 * el qual servirà per determinar el tipus d'instrucció, i un Double, per emmagatzemar
 * el valor d'aquesta.
*/

public class Instruction {

    //Aquí hi ha unes quantes constants que usarem exclusivament en aquesta classe.
    private static final int ZERO = 0;
    private static final int ONE = 1;
    private static final int ONE_THOUSAND = 1000;
    private static final int MAX_DEGREES = 360;

    //Aquí tenim les variables que guardaran el tipus d'instrucció i el valor.
    private String code;
    private Double param;

    //Constructor de la classe.

    public Instruction( String code, Double param){
        this.code = code;
        this.param = param;
    }

    //Funció 'getter' per obtenir el codi (tipus de instrucció)

    public String getCode (){
        return code;
    }

    //Funció 'getter' per obtenir el param (valor de la instrucció)

    public Double getParam (){
        return param;
    }

    //Aquí comprobem que la instrucció sigui de tipus REP o END.

    public Boolean isRepInstruction(){
        return
                code.equals("REP") || code.equals("END");
    }

    //Aquesta funció comproba que la instrucció estigui correcte i en determina el seu codi d'error.

    public int errorCode(){

        if(!(code.equals("REP") || code.equals("FWD") || code.equals("PEN")
                || code.equals("ROT") || code.equals("END")))
            return 1;

        if (code.equals("FWD") && (Math.abs(param) > ONE_THOUSAND))
            return 2;

        if (code.equals("PEN") && (param != ZERO && param != ONE))
            return 3;

        if (code.equals("ROT") && Math.abs(param) > MAX_DEGREES)
            return 4;

        if (code.equals("REP") && (param <= ZERO || param >= ONE_THOUSAND))
            return 5;

        return 0;       //no ha trobat error en el codi
    }

    //Aquesta funció tipo boolean, si la instrucció és correcte o no

    public boolean isCorrect(){
        return
                errorCode() == 0;
    }

    //Aquesta funció tipo String retorna el codi de la instrucció seguit del seu valor, per
    //imprimir la instrucció de cara a l'usuari.

    public String info() {
        return
                code + " " + param.intValue() + "\n";
    }

}
